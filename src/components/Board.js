import React, { Component } from 'react';
import Comment from './Comment'

class Board extends Component {

    constructor(props){
        super(props)
        this.state = {comments:
            [
                'I like bacon',
                'Want to get ice cream?',
                'Ok, we have enough comments'
            ]
        }
    }

    add = (text) => {
        let arr = this.state.comments
        arr.push(text)
        this.setState({
            comments: arr
        })
    }

    removeComment = (i) => {
        let arr = this.state.comments
        arr.splice(i,1)
        this.setState({
            comments: arr
        })
    }

    updateComment = (newText, i) => {
        let arr = this.state.comments
        arr[i] = newText
        this.setState({
            comments: arr
        })
    }

    eachComment = (text, i) => {
        return(<Comment key={i} index={i} updateCommentText={this.updateComment} deleteFromBoard={this.removeComment}>
                {text}
                </Comment>)
        }

    render = () => {
        return (
            <div>
                <button onClick={this.add.bind(null, 'Bacon Tuna')} className="btn-info btn create">Add New</button>
                 <div className="board">
                    {this.state.comments.map(this.eachComment)}
                </div>
            </div>
        )
    }
}

export default Board