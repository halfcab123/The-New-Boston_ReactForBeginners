import React, { Component } from 'react';

class Comment extends Component {

    constructor(props){
        super(props)
        this.state = {
            editing: false
        }
    }

    edit = () => {
        this.setState({ editing: true })
    }

    remove = () => {
        this.props.deleteFromBoard(this.props.index)
    }

    save = () => {
        this.props.updateCommentText(this.refs.newText.value, this.props.index)
        this.setState({ editing: false })
    }

    renderNormal = () => {
        return (
            <div className="container">
                <div className="alert alert-info">
                    <div className="littleBox">
                        <div>{this.props.children}</div>
                        <button onClick={this.edit} type="button" className="right btn btn-primary">Edit</button>
                        <button onClick={this.remove} type="button" className="right btn btn-danger">Remove</button>
                    </div>
                </div>
            </div>
        )
    }

    renderForm = () => {
        return (
            <div className="container">
                <div className="alert alert-info">
                    <div className="littleBox">
                        <textarea ref="newText" defaultValue={this.props.children}></textarea>
                        <button onClick={this.save} type="button" className="right btn btn-success">Save</button>
                    </div>
                </div>
            </div>
        )
    }

    render = () => {
        if(this.state.editing){
            return this.renderForm()
        }
        return this.renderNormal()
    }
}

export default Comment